import os
import requests
from urllib.parse import urljoin
from requests.adapters import HTTPAdapter
import yaml
from requests import RequestException
import logging

LOG = logging.getLogger("DCE_Deploy")

DEFAULT_HEADERS = {
    'User-Agent': "Sail HttpClient",
    'Accept-Encoding': ', '.join(('gzip', 'deflate')),
    'Accept': '*/*',
    'Connection': 'keep-alive',
}


class Error(Exception):
    error_id = "BASE_ERROR"
    error_message = ""

    def __repr__(self):
        return "<{err_id}>: {err_msg}".format(
            err_id=self.error_id,
            err_msg=self.error_message,
        )

    def render(self):
        return {
            "error_id": self.error_id,
            "message": self.error_message,
        }


class DCEClient(requests.Session):
    def __init__(self, base_url, tenant_name, headers: dict = None):
        super(DCEClient, self).__init__()
        self.base_url = base_url
        if not headers:
            headers = {}
        self.headers.update(DEFAULT_HEADERS)
        self.headers.update(headers)
        self.headers['x-dce-tenant'] = tenant_name
        self.mount("http://", HTTPAdapter(max_retries=3))
        self.mount("https://", HTTPAdapter(max_retries=3))

    def url(self, path):
        return urljoin(self.base_url, path)

    @classmethod
    def result_or_raise(cls, response, json=True):
        status_code = response.status_code

        if status_code // 100 != 2:
            msg = "[Status Code {}]: {}".format(status_code, response.text)
            LOG.error(msg)
            raise Error(msg)
        if json:
            return response.json()
        return response.text

    def dce_login(self, username, password):
        try:
            result = self.result_or_raise(self.post(self.url("/dce/login"),
                                                    auth=(username, password), timeout=30))
        except RequestException:
            msg = "Can't connect to DCE."
            LOG.error(msg)
            raise Error(msg)
        access_token = result.get('AccessToken')
        if not access_token:
            msg = "Can't get DCE access token."
            LOG.error(msg)
            raise Error(msg)
        self.headers['X-DCE-Access-Token'] = access_token
        return True

    def check_dce_app_name(self, app_name):
        data = {"Name": app_name, "PrevExist": False}
        try:
            result = self.result_or_raise(
                self.post(self.url("/dce/kube-apps-utils/verify-name"), json=data, timeout=60))
        except Error:
            return False
        if result.get("Id") == "verified":
            return True
        return False

    def deploy_dce_app(self, app_name, yml):
        ymls = yml.split("---\n")
        resources = [yaml.load(y) for y in ymls]
        data = {"AppName": app_name, "Resources": resources}
        result = self.result_or_raise(self.post(self.url("/dce/kube-apps"), json=data, timeout=60))
        errors = result['Errors']
        if len(errors) > 0:
            LOG.warning("\n".join(errors))

    def update_dce_deployed_app(self, app_name, yml):
        ymls = yml.split("---\n")
        resources = [yaml.load(y) for y in ymls]
        data = {"Resources": resources}
        result = self.result_or_raise(
            self.post(self.url("/dce/kube-apps/{}/update-resources".format(app_name)),
                      json=data, timeout=60))
        errors = result['Errors'] or []
        if len(errors) > 0:
            LOG.warning("\n".join(errors))


def main():
    base_url = os.getenv("DCS_CLUSTER_URL")
    tenant_name = os.getenv("DCS_CLUSTER_TENANT_NAME")
    username = os.getenv("DCS_CLUSTER_USERNAME")
    password = os.getenv("DCS_PASSWORD")
    app_name = os.getenv("DCS_APP_NAME")
    deploy_yaml = os.getenv("DCS_DEPLOY_SCRIPT")

    dce_client = DCEClient(base_url, tenant_name)
    dce_client.dce_login(username, password)

    with open(deploy_yaml, "r") as f:
        yml = f.read()
    dce_client.deploy_dce_app(app_name, yml)


if __name__ == "__main__":
    main()
